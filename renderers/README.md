# Renderer files go in this directory  
Pull requests for new renderers are always welcome :)

##How to add a new renderer
1. copy UIKitRenderer.php to a new file
2. rename the file to *FrameWork*Renderer.php, where *Framework* is the name of the framework you want to support, e.g. FoundationRenderer.php
3. rename the class UIkitRenderer to reflect the filename, e.g. `class FoundationRenderer extends Forms\Rendering\DefaultFormRenderer`
3. change the code in construct() and render() methods to reflect the framework's conventions

