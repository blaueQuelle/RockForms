<?php
$info = [
  'title' => 'RockForms Form Helper',
  'version' => 2,
  'summary' => 'Nette Forms Integration to ProcessWire',
  'singular' => false,
  'autoload' => true, // necessary to attach hook for including assets
  'icon' => 'rocket',
];