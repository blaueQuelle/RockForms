<?php namespace ProcessWire;
/**
 * ProcessWire Integrations of NetteForms Framework
 * Bernhard Baumrock, baumrock.com
 * MIT, Removal of the branding in the rendered form is prohibited
 */

class RockForms extends WireData implements Module {
  public function init() {
    $this->form = new Form($this);
    $this->wire->config->RockForms = 0;

    // attach hook to include assets on this page
    $this->wire->addHookAfter('Page::render', function($event) {
      if(!$this->wire->config->RockForms) return;
      $assets = "\t<!-- RockForms -->\n\t".
        "<script src='/site/modules/RockForms/vendor/nette/forms/src/assets/netteForms.js'></script>\n\t".
        "<script src='/site/modules/RockForms/RockForms.js'></script>\n\t".
        "<link rel='stylesheet' type='text/css' href='/site/modules/RockForms/RockForms.css'>\n\t";
      $event->return = str_replace("</head>", "$assets</head>", $event->return);
    });
  }

  public function install() {
    // track this installation
    $http = new WireHttp();
    $http->post('http://www.google-analytics.com/collect', [
      'v' => 1, // Version
      'tid' => 'UA-76905506-1', // Tracking ID / Property ID.
      'cid' => 555, // Anonymous Client ID.
      't' => 'event', // hit type
      'ec' => 'PWModules', // category
      'ea' => 'install', // action
      'el' => $this->className, // label
    ]);
  }
  public function uninstall() {
    // track this uninstallation
    $http = new WireHttp();
    $http->post('http://www.google-analytics.com/collect', [
      'v' => 1, // Version
      'tid' => 'UA-76905506-1', // Tracking ID / Property ID.
      'cid' => 555, // Anonymous Client ID.
      't' => 'event', // hit type
      'ec' => 'PWModules', // category
      'ea' => 'uninstall', // action
      'el' => $this->className, // label
    ]);
  }
}

/**
 * extend the nette form to our needs
 */
require_once(__DIR__ . '/vendor/autoload.php');
class Form extends \Nette\Forms\Form {
  public $framework = 'UIKit';
  public $showLabels = true;
  private $wire;
  private $module;
  public $honeypot = 'soft';
  public $markup = '';
  public $linkedFields = []; // link fields of form to fields of processwire page

  public function __construct($module) {
    $this->module = $module;
    $this->wire = $module->wire;
    $this->onSuccess = function() { throw new WireException("onSuccess function must be defined"); };
  }

  /**
   * render the form or the success message
   */
  public function render(...$args) {
    $post = $this->wire->input->post;
    $session = $this->wire->session;
    $this->addRockForms();

    // increase the count of rockforms on this page
    $this->wire->config->RockForms++;
    
    // CSRF check (only when form was submitted)
    if($post->RockForms) {
      try { $session->CSRF->validate(); }
      catch(WireCSRFException $e) {
        $this->addError(__("Form was not submitted due to CSRF protection"));
      }
      $session->CSRF->resetToken();
    }

    // make sure the url with the success parameter is only accessible via real form submits
    if($this->wire->input->get('success')) {
      if($msg = $session->RockFormsSuccessMsg) {
        // the form was submitted successfully
        // reset session flag
        $session->RockFormsSuccessMsg = null;
        return '<div id="RockForms_' . $this->getElementPrototype()->id . '">' . $msg . '</div>';
      }
      else {
        // there was a success parameter in the url but no form was submitted
        // redirect this fake parameter to the original page url
        $session->redirect($this->wire->page->url);
      }
    }

    // handle successful form submits
    if($this->isSuccess()) {
      if(!$post->RockForms) throw new Exception("Invalid request");

      $session->RockFormsSuccessMsg = $this->onSuccess->__invoke($this);
      $url = rtrim($this->wire->page->url, '/');
      $session->redirect($url . '/' . $this->formSuccessParameter());
    }

    $this->addCSRF();
    $this->setFramework($this->framework);
    $this->setAction('./');
    $form = $this->markup;
    if(!$form) {
      // if no markup was set, we get the markup created by nette
      // this needs to be read from buffer since the form render() echos markup
      ob_start();
      parent::render($args);
      $form = ob_get_clean();
    }

    return $form.
      '<div class="RockFormsBranding"><a href="https://gitlab.com/baumrock/RockForms">RockForms</a> made with &hearts; in Vienna by <a href="https://www.baumrock.com">baumrock.com</a></div>';
  }

  /**
   * url parameter for tracking (analytics)
   */
  private function formSuccessParameter() {
    $id = $this->getElementPrototype()->id ?: uniqid();
    return "?success=$id";
  }

  /**
   * add form elements necessary to work for rockforms
   */
  private function addRockForms() {
    $this->addHidden('RockForms', 1);
    // add honeypot fields to the form
    foreach(explode("\n", $this->module->nettehny) as $name) {
      if(!$name) continue;
      $this->addText($name)
        ->addRule($this::BLANK, __('Leave this field empty!'))
        ->setOmitted(true)
        ->setAttribute('class', 'nettehny')
        ->setAttribute('autocomplete', 'off')
        ;
        
      // add this field to the custom form markup if it was set
      if(strpos($this->markup, '</form>') !== false)
        $this->markup = str_replace('</form>', $this->getComponent($name)->control . '</form>', $this->markup);
      elseif($this->markup)
        $this->markup .= $this->getComponent($name)->control;
    }
  }
  
  /**
   * set the form's renderer
   * @param String $framework name of the framework
   */
  public function setFramework($framework) {
    // add names to control element
    foreach($this->getComponents() as $component) {
      $component->getControlPrototype()->addClass('RockFormsControl');
    }
    // include renderer file and set renderer
    $rendererName = "{$framework}Renderer";
    $rendererPathAssets = wire('config')->paths->assets . "RockForms/{$rendererName}.php";
    $rendererPathModule = __DIR__ . "/renderers/{$rendererName}.php";
    $className = __NAMESPACE__ . '\\' . $rendererName;
    if(file_exists($rendererPathAssets)) {
      require_once($rendererPathAssets);
    } elseif(file_exists($rendererPathModule)) {
      require_once($rendererPathModule);
    } else {
      $rendererFiles = $this->wire->files->find(__DIR__ . '/renderers/', ['extensions' => ['php']]);
      $renderers = array();
      foreach ($rendererFiles as $f) $renderers[] = str_replace('Renderer', '', basename($f, '.php'));
      throw new WireException("The renderer {$this->framework} does not exist. Available renderers are: " . implode(', ', $renderers));
    }
    $this->setRenderer(new $className($this));
  }

  /**
   * add processwires csrf protection du to this bug
   * https://github.com/nette/forms/issues/154
   */
  private function addCSRF() {
    $this->addHidden($this->wire->session->CSRF->getTokenName())->setValue($this->wire->session->CSRF->getTokenValue());
  }

  /**
   * link form fields to pw page fields
   */
  public function link($oldkey, $newkey) {
    $arr = $this->linkedValues ?: $this->getValues();
    if(!isset($arr->{$oldkey})) throw new WireException("$oldkey not found");
    if(isset($arr->{$newkey})) throw new WireException("$newkey is already present");

    $oldval = $arr->{$oldkey};
    unset($arr->{$oldkey});
    $arr->{$newkey} = $oldval;
    $this->linkedValues = $arr;
    return $this;
  }

  /**
   * create processwire page to log this form submission
   * @param string|Page $parent, parent id or processwire page
   * @param string|Template $template, new pages template
   */
  public function createPage($parent, $template, $title = null) {
    $newparent = $this->wire->pages->get((string)$parent);
    if(!$newparent->id) throw new WireException("Invalid parent $parent");
    $parent = $newparent;

    $newtemplate = $this->wire->templates->get((string)$template);
    if(!$newtemplate->id) throw new WireException("Invalid template $template");
    $template = $newtemplate;

    $page = new Page();
    $page->parent = $parent;
    $page->template = $template;
    $page->title = $title ?: uniqid();
    $page->save();

    foreach($this->getValues() as $field => $value) {
      $pwfield = $field;
      if(isset($this->linkedFields[$field])) $pwfield = $this->linkedFields[$field];

      $component = $this->getComponent($field);
      switch($component->getOption('type')) {
        case 'file':
          // upload image
          $tmpfilename = $value->getTemporaryFile();
          if(!$tmpfilename) continue;
          $newbasename = $this->wire->sanitizer->fileName($value->getName());

          $tmp = new WireTempDir('RockForms');
          $newfilename = (string)$tmp . $newbasename;
          move_uploaded_file($tmpfilename, $newfilename);
          $page->{$pwfield}->add($newfilename);
          break;
        
        default:
          $page->{$pwfield} = $value;
          $page->title = str_replace('{'.$field.'}', $value, $page->title);
          break;
      }
    }

    $page->save();

    return $page;
  }

  /**
   * render table of submitted form
   */
  public function renderTable($options = []) {
    // default settings for this function
    $defaults = [
      // here you can define fields to skip for rendering
      'skip' => [],

      // here you can define the wrapper for the table
      // for example you can render the table in a uk-card
      'wrapper' => '<div>{table}</div>',
    ];
    $options = array_merge($defaults, $options);

    // always skip RockForms field
    $options['skip'][] = 'RockForms';

    // define variables
    $hr = '';
    $out = '';

    // render all controls of this form
    if(count($this->getGroups())) {
      foreach($this->getGroups() as $group) {
        $out .= $hr;
        $hr = '<hr>';
        if($this->getGroupLabel($group))
          $out .= '<h4>' . $this->getGroupLabel($group) . '</h4>';

        $out .= $this->renderControls($group->getControls(), $options);
      }
    }
    else {
      // render all controls of this form
      $out .= $this->renderControls($this->getComponents(), $options);
    }
    
    // return table + wrapper
    return str_replace("{table}", $out, $options['wrapper']);
  }

  /**
   * render controls for table output
   */
  private function renderControls($controls, $options) {
    $paddingleft = 'style="padding-left: 10px;"';
    $values = $this->getValues();

    $out = '<table style="text-align: left;">';
    foreach($controls as $control) {
      $field = $control->name;
      if(in_array($field, $options['skip'])) continue;

      // define vars
      $component = $this->getComponent($field);
      $out .= '<tr>';


      // todo
      // craete own methods to retrieve the label and the value to reduce redundancy

      // switch field type
      switch($component->getOption('type')) {
        case 'file':
          $value = isset($values[$field]) ? $values[$field] : '';
          if(is_string($value)) break;
          $out .= "<td>{$this->getFieldLabel($field)}</td>";
          $out .= "<td $paddingleft>{$value->getName()}</td>";
          break;

        case 'checkbox':
          if($values[$field]) $out .= "<td colspan='2'>{$control->caption}</td>";
          break;

        case 'radio':
          $out .= "<td>{$this->getFieldLabel($field)}</td>";

          $value = isset($values[$field]) ? $component->items[$values[$field]] : '';
          $out .= "<td $paddingleft>$value</td>";
          break;

        case 'button':
          // skip buttons
          break;
        
        default:
          $out .= "<td>{$this->getFieldLabel($field)}</td>";

          $value = isset($values[$field]) ? $values[$field] : '';
          $out .= "<td $paddingleft>".nl2br($value)."</td>";
          break;
      }

      $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
  }

  /**
   * return field label for table rendering
   */
  private function getFieldLabel($fieldname) {
    $component = $this->getComponent($fieldname);
    return ($component->label) ? $component->label->getText() : $fieldname;
  }

  private function getGroupLabel($group) {
    if(!$group) return '';
    if(!$group->getOption('label')) return '';
    return $group->getOption('label')->getText();
  }

  /**
   * Convenience method for setting attributes to the <form> tag
   * @param String $attr can be any valid HTML attribute like class, id, data-something, aria-something
   * @param String $val value for the attribute
   */

  public function setAttribute($attr, $val) {
    $method = 'set' . ucfirst($attr);
    $this->form->getElementPrototype()->$method($val);
  }
}
